package net.mca.vaultadapter.economy;

import lombok.extern.apachecommons.CommonsLog;
import net.mca.vaultadapter.util.ReflectionUtil;
import org.bukkit.OfflinePlayer;
import net.mca.vaultadapter.util.PlayerUtil;
import net.mca.vaultadapter.util.PluginUtil;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@CommonsLog
public class EconomyAdapter {
    private static final String econPath = "net.milkbowl.vault.economy.Economy";
    private static final String econResponsePath = "net.milkbowl.vault.economy.EconomyResponse";

    /**
     * singleton
     */
    private static EconomyAdapter instance;

    private Object econ = null;
    private Map<String, Method> methods = new HashMap<String, Method>();

    public static void initAdapter() {
        try {
            PluginUtil.checkVault();

            EconomyAdapter eco = new EconomyAdapter();

            eco.econ = PluginUtil.getSerivce(ReflectionUtil.getVaultClass(econPath));

            eco.methods.put("getName", ReflectionUtil.getVaultMethod(econPath, "getName"));
            eco.methods.put("isEnabled", ReflectionUtil.getVaultMethod(econPath, "isEnabled"));
            eco.methods.put("getBalance", ReflectionUtil.getVaultMethod(econPath, "getBalance", OfflinePlayer.class));
            eco.methods.put("createPlayerAccount", ReflectionUtil.getVaultMethod(econPath, "createPlayerAccount", OfflinePlayer.class));
            eco.methods.put("withdrawPlayer", ReflectionUtil.getVaultMethod(econPath, "withdrawPlayer", OfflinePlayer.class, double.class));
            eco.methods.put("depositPlayer", ReflectionUtil.getVaultMethod(econPath, "depositPlayer", OfflinePlayer.class, double.class));
            eco.methods.put("has", ReflectionUtil.getVaultMethod(econPath, "has", OfflinePlayer.class, double.class));
            eco.methods.put("hasAccount", ReflectionUtil.getVaultMethod(econPath, "hasAccount", OfflinePlayer.class));

            eco.methods.put("transactionSuccess", ReflectionUtil.getVaultMethod(econResponsePath, "transactionSuccess"));

            instance = eco;
        } catch (Exception e) {
            log.error("Error on init " + EconomyAdapter.class.getSimpleName() + "!");
            e.printStackTrace();
        }
    }

    public static boolean hasInit() {
        return instance != null;
    }

    public static EconomyAdapter getInstance() {
        return instance;
    }

    public String getCurrentEconomyPluginName() {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getName"), this.econ);
    }

    public boolean currentEconomyPluginIsEnabled() {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("isEnabled"), this.econ);
    }

    public double getBalance(String playerName) {
        return (Double) ReflectionUtil.invokeSafety(this.methods.get("getBalance"), this.econ, PlayerUtil.getOfflinePlayer(playerName));
    }

    public boolean createPlayerAccount(String playerName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("createPlayerAccount"), this.econ, PlayerUtil.getOfflinePlayer(playerName));
    }

    public boolean withdrawPlayer(String playerName, double amount) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("transactionSuccess"), ReflectionUtil.invokeSafety(this.methods.get("withdrawPlayer"), this.econ, PlayerUtil.getOfflinePlayer(playerName), amount));
    }

    public boolean depositPlayer(String playerName, double amount) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("transactionSuccess"), ReflectionUtil.invokeSafety(this.methods.get("depositPlayer"), this.econ, PlayerUtil.getOfflinePlayer(playerName), amount));
    }

    public boolean playerHasMoney(String playerName, double amount) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("has"), PlayerUtil.getOfflinePlayer(playerName), amount);
    }

    public boolean hasAccount(String playerName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("hasAccount"), PlayerUtil.getOfflinePlayer(playerName));
    }

}
