package net.mca.vaultadapter.command;

import net.mca.vaultadapter.permission.PermissionAdapter;
import net.minecraft.command.ICommandSender;

public class CommandPermission extends CommandSub {
    @Override
    public String getSubCommand() {
        return "perms";
    }

    @Override
    public String getSubCommandUsage() {
        return Color.VALUE + "/vault perms help - "
                + Color.INFO + "Display permission commands.";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            CommandSub.printMessageToSender(sender, this.getSubCommandUsage());
            return;
        }

        String action = args[0];

        if (action.equalsIgnoreCase("help")) {
            CommandSub.printMessageToSender(sender, Color.INFO + "==="
                    + Color.VALUE + "VaultAdapter v.1.0.0 [Perms]"
                    + Color.INFO + "===");
            CommandSub.printMessageToSender(sender, "");
            CommandSub.printMessageToSender(sender, Color.VALUE + "Commands:");
            CommandSub.printMessageToSender(sender, Color.VALUE + "/vault perms help - "
                    + Color.INFO + "Display permission commands.");
            CommandSub.printMessageToSender(sender, Color.VALUE + "/vault perms plugin - "
                    + Color.INFO + "Display current perms plugin.");
            CommandSub.printMessageToSender(sender, Color.VALUE + "/vault perms groups - "
                    + Color.INFO + "Display current available groups.");
            CommandSub.printMessageToSender(sender, Color.VALUE + "/vault perms groups [name] - "
                    + Color.INFO + "Display player's groups.");


            return;
        }

        if (action.equalsIgnoreCase("plugin")) {
            CommandSub.printMessageToSender(sender, Color.INFO + "Current permission plugin: "
                    + Color.VALUE + PermissionAdapter.getInstance().getCurrentPermissionPluginName());

            return;
        }

        if (action.equalsIgnoreCase("groups") && args.length == 1) {
            CommandSub.printMessageToSender(sender, Color.INFO + "Current groups in "
                    + Color.VALUE + PermissionAdapter.getInstance().getCurrentPermissionPluginName() + ":");

            for (String groupName : PermissionAdapter.getInstance().getGroups()) {
                CommandSub.printMessageToSender(sender, Color.ITEM + "   " + groupName);
            }

            return;
        }

        if (args.length < 2) {
            CommandSub.printMessageToSender(sender, this.getSubCommandUsage());

            return;
        }

        String name = args[1];

        if (action.equalsIgnoreCase("groups")) {
            CommandSub.printMessageToSender(sender, Color.VALUE + name
                    + Color.INFO + "'s groups:");
            CommandSub.printMessageToSender(sender, Color.INFO + "Main:");
            CommandSub.printMessageToSender(sender, "   " + Color.VALUE + PermissionAdapter.getInstance().getPrimaryGroup(name));
            CommandSub.printMessageToSender(sender, Color.INFO + "Other: ");

            for (String groupName : PermissionAdapter.getInstance().getPlayerGroups(name)) {
                if (groupName.equals(PermissionAdapter.getInstance().getPrimaryGroup(name))) {
                    continue;
                }

                CommandSub.printMessageToSender(sender, "   " + Color.VALUE + groupName);
            }
        }
    }

}
