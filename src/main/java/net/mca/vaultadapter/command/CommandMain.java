package net.mca.vaultadapter.command;

import net.mca.vaultadapter.command.CommandSub.Color;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CommandMain extends CommandBase {
    private List<CommandSub> commands = new ArrayList<CommandSub>();

    public CommandMain() {
        this.commands.add(new CommandPermission());
    }

    @Override
    public String getCommandName() {
        return "vault";
    }

    public List<String> getCommandAliases() {
        List<String> aliases = new ArrayList<String>();

        return aliases;
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return Color.VALUE + "/vault help - "
                + Color.INFO + "Display mod commands.";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.addChatMessage(new ChatComponentText(getCommandUsage(sender)));
            return;
        }

        for (CommandSub command : this.commands) {
            if (args[0].equalsIgnoreCase(command.getSubCommand())) {
                command.processCommand(sender, Arrays.copyOfRange(args, 1, args.length));
                return;
            }
        }

        sender.addChatMessage(new ChatComponentText(getCommandUsage(sender)));
    }

}
