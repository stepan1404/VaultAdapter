package net.mca.vaultadapter.command;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IChatComponent;

public abstract class CommandSub {
    public abstract String getSubCommand();

    public abstract String getSubCommandUsage();

    public abstract void processCommand(ICommandSender sender, String[] args);

    static void printMessageToSender(ICommandSender sender, String text) {
        IChatComponent message = new ChatComponentText(text);

        sender.addChatMessage(message);
    }

    public enum Color {
        ERROR(EnumChatFormatting.RED),
        INFO(EnumChatFormatting.AQUA),
        VALUE(EnumChatFormatting.GOLD),
        ITEM(EnumChatFormatting.BLUE);

        private EnumChatFormatting chatFormatter;

        Color(EnumChatFormatting chatFormatter) {
            this.chatFormatter = chatFormatter;
        }

        public String toString() {
            return this.chatFormatter.toString();
        }
    }

}
