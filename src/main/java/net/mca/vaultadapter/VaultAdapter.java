package net.mca.vaultadapter;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;
import net.mca.vaultadapter.chat.ChatAdapter;
import net.mca.vaultadapter.command.CommandMain;
import net.mca.vaultadapter.config.Config;
import net.mca.vaultadapter.economy.EconomyAdapter;
import net.mca.vaultadapter.permission.PermissionAdapter;
import net.mca.vaultadapter.util.PluginUtil;

@Mod(modid = "VaultAdapter", name = "VaultAdapter", version = "1.0.0", useMetadata = true, acceptableRemoteVersions = "*")
@CommonsLog
public class VaultAdapter {
    @Getter
    private static VaultAdapter instance;
    @Getter
    private Config config;

    @SideOnly(Side.SERVER)
    @EventHandler
    public void onPreInit(FMLPreInitializationEvent e) {
        instance = this;
        this.config = new Config(e.getSuggestedConfigurationFile());
    }

    @SideOnly(Side.SERVER)
    @EventHandler
    public void onServerStarting(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandMain());
    }

    @SideOnly(Side.SERVER)
    @EventHandler
    public void onServerStarted(FMLServerStartedEvent event) {
        log.info("Starting VaultAdapter!");
        try {
            PluginUtil.checkBukkit();
            new Thread("VaultAdapter Initializer") {
                @Override
                public void run() {
                    while ((!ChatAdapter.hasInit() && config.isEnableChat()) || (!EconomyAdapter.hasInit() && config.isEnableEconomy()) || (!PermissionAdapter.hasInit() && config.isEnablePermission())) {
                        if (!ChatAdapter.hasInit()) {
                            ChatAdapter.initAdapter();
                            log.info("ChatAdapter is initalized!");
                        }
                        if (!EconomyAdapter.hasInit()) {
                            EconomyAdapter.initAdapter();
                            log.info("EconomyAdapter is initalized!");
                        }
                        if (!PermissionAdapter.hasInit()) {
                            PermissionAdapter.initAdapter();
                            log.info("PermissionAdapter is initalized!");
                        }
                    }
                }
            }.start();
        } catch (ClassNotFoundException exc) {
            log.info("This core isn't supporting Bukkit platform!");
            exc.printStackTrace();
        }
    }
}
