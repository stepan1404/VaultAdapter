package net.mca.vaultadapter.config;

import lombok.Getter;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class Config {
    private Configuration configuration;
    @Getter
    private boolean enableEconomy;
    @Getter
    private boolean enableChat;
    @Getter
    private boolean enablePermission;

    public Config(File file) {
        this.configuration = new Configuration(file);
        this.reload();
    }

    private void reload() {
        this.configuration.load();

        this.enableEconomy = this.configuration.getBoolean("economy", "toggle", true, "Set 'true' or 'false' for toggle enabling EconomyAdapter");
        this.enableChat = this.configuration.getBoolean("chat", "toggle", true, "Set 'true' or 'false' for toggle enabling ChatAdapter");
        this.enablePermission = this.configuration.getBoolean("permission", "toggle", true, "Set 'true' or 'false' for toggle enabling PermissionAdapter");

        this.configuration.save();
    }
}
