package net.mca.vaultadapter.util;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class PlayerUtil {
    @SuppressWarnings("unchecked")
    public static EntityPlayer getPlayer(String name) {
        return ((List<EntityPlayer>) MinecraftServer.getServer().getConfigurationManager().playerEntityList)
                .stream().filter(ep -> ep.getGameProfile().getName().equals(name)).findFirst().orElse(null);
    }

    public static Player getBukkitPlayer(String uuid) {
        return Bukkit.getPlayer(UUID.fromString(uuid));
    }

    public static OfflinePlayer getOfflinePlayer(String name) {
        return Bukkit.getOfflinePlayer(name);
    }
}
