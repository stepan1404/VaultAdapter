package net.mca.vaultadapter.util;

import net.minecraft.server.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.World;

public class WorldUtil {
    private static final String DEFAULT_WORLD_NAME = MinecraftServer.getServer().getFolderName();

    public static World getDefaultWorld() {
        return Bukkit.getWorld(DEFAULT_WORLD_NAME);
    }

    /**
     * Return universal value ("null", lol) that mark all worlds
     *
     * @return <b>null</b>
     */
    public static String getAllNames() {
        return null;
    }
}
