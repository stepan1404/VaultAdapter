package net.mca.vaultadapter.util;

import lombok.extern.apachecommons.CommonsLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

@CommonsLog
public class ReflectionUtil {
    public static Class<?> getVaultClass(String path) {
        try {
            return Class.forName(path, true, ReflectionUtil.getVaultClassLoader());
        } catch (ClassNotFoundException e) {
            log.error("Class " + path + " not found!");
            e.printStackTrace();
        }
        return null;
    }

    private static ClassLoader getVaultClassLoader() {
        return PluginUtil.getVault().getClass().getClassLoader();
    }

    public static Method getVaultMethod(String classPath, String methodName, Class<?>... parameterTypes) {
        try {
            return Objects.requireNonNull(ReflectionUtil.getVaultClass(classPath)).getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            log.error("Method " + methodName + " not found in " + classPath + "!");
            e.printStackTrace();
        } catch (SecurityException e) {
            log.error("Security Exception!");
            e.printStackTrace();
        }
        return null;
    }

    public static Object invokeSafety(Method method, Object object, Object... params) {
        try {
            return method.invoke(object, params);
        } catch (IllegalAccessException e) {
            log.error("Access Exception!");
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            log.error("Argument Exception!");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            log.error("Invocation Target Exception!");
            e.printStackTrace();
        }
        return null;
    }
}
