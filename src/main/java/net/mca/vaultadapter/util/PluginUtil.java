package net.mca.vaultadapter.util;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

public class PluginUtil {
    public static void checkVault() throws Exception {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
            throw new Exception();
        }
    }

    public static Plugin getVault() {
        return Bukkit.getPluginManager().getPlugin("Vault");
    }

    public static void checkBukkit() throws ClassNotFoundException {
        Class.forName("org.bukkit.Bukkit");
    }

    public static <T> T getSerivce(Class<T> clazz) throws Exception {
        RegisteredServiceProvider<T> rsp = Bukkit.getServer().getServicesManager().getRegistration(clazz);

        if (rsp == null) {
            throw new Exception();
        }

        return rsp.getProvider();
    }
}
