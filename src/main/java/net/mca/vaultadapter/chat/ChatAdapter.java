package net.mca.vaultadapter.chat;

import lombok.extern.apachecommons.CommonsLog;
import net.mca.vaultadapter.util.ReflectionUtil;
import org.bukkit.OfflinePlayer;
import net.mca.vaultadapter.util.PlayerUtil;
import net.mca.vaultadapter.util.PluginUtil;
import net.mca.vaultadapter.util.WorldUtil;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@CommonsLog
public class ChatAdapter {
    private static final String chatPath = "net.milkbowl.vault.chat.Chat";

    /**
     * singleton
     */
    private static ChatAdapter instance;

    private Object chat = null;
    private Map<String, Method> methods = new HashMap<String, Method>();


    public static void initAdapter() {
        try {
            PluginUtil.checkVault();

            ChatAdapter chat = new ChatAdapter();

            chat.chat = PluginUtil.getSerivce(ReflectionUtil.getVaultClass(chatPath));

            chat.methods.put("getGroupInfoBoolean", ReflectionUtil.getVaultMethod(chatPath, "getGroupInfoBoolean", String.class, String.class, String.class, boolean.class));
            chat.methods.put("getGroupInfoInteger", ReflectionUtil.getVaultMethod(chatPath, "getGroupInfoInteger", String.class, String.class, String.class, int.class));
            chat.methods.put("getGroupInfoDouble", ReflectionUtil.getVaultMethod(chatPath, "getGroupInfoDouble", String.class, String.class, String.class, double.class));
            chat.methods.put("getGroupInfoString", ReflectionUtil.getVaultMethod(chatPath, "getGroupInfoString", String.class, String.class, String.class, String.class));
            chat.methods.put("getGroupPrefix", ReflectionUtil.getVaultMethod(chatPath, "getGroupPrefix", String.class, String.class));
            chat.methods.put("getGroupSuffix", ReflectionUtil.getVaultMethod(chatPath, "getGroupSuffix", String.class, String.class));
            chat.methods.put("getName", ReflectionUtil.getVaultMethod(chatPath, "getName"));
            chat.methods.put("isEnabled", ReflectionUtil.getVaultMethod(chatPath, "isEnabled"));
            chat.methods.put("getPlayerInfoBoolean", ReflectionUtil.getVaultMethod(chatPath, "getPlayerInfoBoolean", String.class, OfflinePlayer.class, String.class, boolean.class));
            chat.methods.put("getPlayerInfoInteger", ReflectionUtil.getVaultMethod(chatPath, "getPlayerInfoInteger", String.class, OfflinePlayer.class, String.class, int.class));
            chat.methods.put("getPlayerInfoDouble", ReflectionUtil.getVaultMethod(chatPath, "getPlayerInfoDouble", String.class, OfflinePlayer.class, String.class, double.class));
            chat.methods.put("getPlayerInfoString", ReflectionUtil.getVaultMethod(chatPath, "getPlayerInfoString", String.class, OfflinePlayer.class, String.class, String.class));
            chat.methods.put("getPlayerPrefix", ReflectionUtil.getVaultMethod(chatPath, "getPlayerPrefix", String.class, OfflinePlayer.class));
            chat.methods.put("getPlayerSuffix", ReflectionUtil.getVaultMethod(chatPath, "getPlayerSuffix", String.class, OfflinePlayer.class));
            chat.methods.put("setGroupInfoBoolean", ReflectionUtil.getVaultMethod(chatPath, "setGroupInfoBoolean", String.class, String.class, String.class, boolean.class));
            chat.methods.put("setGroupInfoInteger", ReflectionUtil.getVaultMethod(chatPath, "setGroupInfoInteger", String.class, String.class, String.class, int.class));
            chat.methods.put("setGroupInfoDouble", ReflectionUtil.getVaultMethod(chatPath, "setGroupInfoDouble", String.class, String.class, String.class, double.class));
            chat.methods.put("setGroupInfoString", ReflectionUtil.getVaultMethod(chatPath, "setGroupInfoString", String.class, String.class, String.class, String.class));
            chat.methods.put("setGroupPrefix", ReflectionUtil.getVaultMethod(chatPath, "setGroupPrefix", String.class, String.class, String.class));
            chat.methods.put("setGroupSuffix", ReflectionUtil.getVaultMethod(chatPath, "setGroupSuffix", String.class, String.class, String.class));
            chat.methods.put("setPlayerInfoBoolean", ReflectionUtil.getVaultMethod(chatPath, "setPlayerInfoBoolean", String.class, OfflinePlayer.class, String.class, boolean.class));
            chat.methods.put("setPlayerInfoInteger", ReflectionUtil.getVaultMethod(chatPath, "setPlayerInfoInteger", String.class, OfflinePlayer.class, String.class, int.class));
            chat.methods.put("setPlayerInfoDouble", ReflectionUtil.getVaultMethod(chatPath, "setPlayerInfoDouble", String.class, OfflinePlayer.class, String.class, double.class));
            chat.methods.put("setPlayerInfoString", ReflectionUtil.getVaultMethod(chatPath, "setPlayerInfoString", String.class, OfflinePlayer.class, String.class, String.class));
            chat.methods.put("setPlayerPrefix", ReflectionUtil.getVaultMethod(chatPath, "setPlayerPrefix", String.class, OfflinePlayer.class, String.class));
            chat.methods.put("setPlayerSuffix", ReflectionUtil.getVaultMethod(chatPath, "setPlayerSuffix", String.class, OfflinePlayer.class, String.class));

            instance = chat;
        } catch (Exception e) {
            log.error("Error on init " + ChatAdapter.class.getSimpleName() + "!");
            e.printStackTrace();
        }
    }

    public static boolean hasInit() {
        return instance != null;
    }

    public static ChatAdapter getInstance() {
        return instance;
    }

    public boolean getGroupInfoBoolean(String group, String node, boolean defaultValue) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("getGroupInfoBoolean"), this.chat, WorldUtil.getAllNames(), group, node, defaultValue);
    }

    public int getGroupInfoInteger(String group, String node, int defaultValue) {
        return (Integer) ReflectionUtil.invokeSafety(this.methods.get("getGroupInfoInteger"), this.chat, WorldUtil.getAllNames(), group, node, defaultValue);
    }

    public double getGroupInfoDouble(String group, String node, double defaultValue) {
        return (Double) ReflectionUtil.invokeSafety(this.methods.get("getGroupInfoDouble"), this.chat, WorldUtil.getAllNames(), group, node, defaultValue);
    }

    public String getGroupInfoString(String group, String node, String defaultValue) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getGroupInfoString"), this.chat, WorldUtil.getAllNames(), group, node, defaultValue);
    }

    public String getGroupPrefix(String group) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getGroupPrefix"), this.chat, WorldUtil.getAllNames(), group);
    }

    public String getGroupSuffix(String group) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getGroupSuffix"), this.chat, WorldUtil.getAllNames(), group);
    }

    public String getCurrentChatPluginName() {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getName"), this.chat);
    }

    public boolean currenChatPluginIsEnabled() {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("isEnabled"), this.chat);
    }

    public boolean getPlayerInfoBoolean(String name, String node, boolean defaultValue) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("getPlayerInfoBoolean"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, defaultValue);
    }

    public int getPlayerInfoInteger(String name, String node, int defaultValue) {
        return (Integer) ReflectionUtil.invokeSafety(this.methods.get("getPlayerInfoInteger"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, defaultValue);
    }

    public double getPlayerInfoDouble(String name, String node, double defaultValue) {
        return (Double) ReflectionUtil.invokeSafety(this.methods.get("getPlayerInfoDouble"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, defaultValue);
    }

    public String getPlayerInfoString(String name, String node, String defaultValue) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getPlayerInfoString"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, defaultValue);
    }

    public String getPlayerPrefix(String name) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getPlayerPrefix"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name));
    }

    public String getPlayerSuffix(String name) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getPlayerSuffix"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name));
    }

    public void setGroupInfoBoolean(String name, String node, boolean value) {
        ReflectionUtil.invokeSafety(this.methods.get("setGroupInfoBoolean"), this.chat, WorldUtil.getAllNames(), name, node, value);
    }

    public void setGroupInfoInteger(String name, String node, int value) {
        ReflectionUtil.invokeSafety(this.methods.get("setGroupInfoInteger"), this.chat, WorldUtil.getAllNames(), name, node, value);
    }

    public void setGroupInfoDouble(String name, String node, double value) {
        ReflectionUtil.invokeSafety(this.methods.get("setGroupInfoDouble"), this.chat, WorldUtil.getAllNames(), name, node, value);
    }

    public void setGroupInfoString(String name, String node, String value) {
        ReflectionUtil.invokeSafety(this.methods.get("setGroupInfoString"), this.chat, WorldUtil.getAllNames(), name, node, value);
    }

    public void setGroupPrefix(String groupName, String prefix) {
        ReflectionUtil.invokeSafety(this.methods.get("setGroupPrefix"), this.chat, WorldUtil.getAllNames(), groupName, prefix);
    }

    public void setGroupSuffix(String groupName, String suffix) {
        ReflectionUtil.invokeSafety(this.methods.get("setGroupSuffix"), this.chat, WorldUtil.getAllNames(), groupName, suffix);
    }

    public void setPlayerInfoInteger(String name, String node, int value) {
        ReflectionUtil.invokeSafety(this.methods.get("setPlayerInfoInteger"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, value);
    }

    public void setPlayerInfoBoolean(String name, String node, boolean value) {
        ReflectionUtil.invokeSafety(this.methods.get("setPlayerInfoBoolean"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, value);
    }

    public void setPlayerInfoDouble(String name, String node, double value) {
        ReflectionUtil.invokeSafety(this.methods.get("setPlayerInfoDouble"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, value);
    }

    public void setPlayerInfoString(String name, String node, String value) {
        ReflectionUtil.invokeSafety(this.methods.get("setPlayerInfoString"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), node, value);
    }

    public void setPlayerPrefix(String name, String prefix) {
        ReflectionUtil.invokeSafety(this.methods.get("setPlayerPrefix"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), prefix);
    }

    public void setPlayerSuffix(String name, String suffix) {
        ReflectionUtil.invokeSafety(this.methods.get("setPlayerSuffix"), this.chat, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), suffix);
    }

}
