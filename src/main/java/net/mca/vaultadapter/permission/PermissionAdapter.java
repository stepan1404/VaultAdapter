package net.mca.vaultadapter.permission;

import lombok.extern.apachecommons.CommonsLog;
import net.mca.vaultadapter.util.PlayerUtil;
import net.mca.vaultadapter.util.PluginUtil;
import net.mca.vaultadapter.util.ReflectionUtil;
import org.bukkit.OfflinePlayer;
import net.mca.vaultadapter.util.WorldUtil;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@CommonsLog
public class PermissionAdapter {
    private static final String permsPath = "net.milkbowl.vault.permission.Permission";

    /**
     * singleton
     */
    private static PermissionAdapter instance;

    private Object perms = null;
    private Map<String, Method> methods = new HashMap<String, Method>();

    public static void initAdapter() {
        try {
            PluginUtil.checkVault();

            PermissionAdapter perm = new PermissionAdapter();

            perm.perms = PluginUtil.getSerivce(ReflectionUtil.getVaultClass(permsPath));

            perm.methods.put("getName", ReflectionUtil.getVaultMethod(permsPath, "getName"));
            perm.methods.put("isEnabled", ReflectionUtil.getVaultMethod(permsPath, "isEnabled"));
            perm.methods.put("playerHas", ReflectionUtil.getVaultMethod(permsPath, "playerHas", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("getGroups", ReflectionUtil.getVaultMethod(permsPath, "getGroups"));
            perm.methods.put("getPrimaryGroup", ReflectionUtil.getVaultMethod(permsPath, "getPrimaryGroup", String.class, OfflinePlayer.class));
            perm.methods.put("groupHas", ReflectionUtil.getVaultMethod(permsPath, "groupHas", String.class, String.class, String.class));
            perm.methods.put("groupAdd", ReflectionUtil.getVaultMethod(permsPath, "groupAdd", String.class, String.class, String.class));
            perm.methods.put("getPlayerGroups", ReflectionUtil.getVaultMethod(permsPath, "getPlayerGroups", String.class, OfflinePlayer.class));
            perm.methods.put("groupRemove", ReflectionUtil.getVaultMethod(permsPath, "groupRemove", String.class, String.class, String.class));
            perm.methods.put("playerAdd", ReflectionUtil.getVaultMethod(permsPath, "playerAdd", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("playerAddGroup", ReflectionUtil.getVaultMethod(permsPath, "playerAddGroup", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("playerAddTransient", ReflectionUtil.getVaultMethod(permsPath, "playerAddTransient", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("playerInGroup", ReflectionUtil.getVaultMethod(permsPath, "playerInGroup", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("playerRemove", ReflectionUtil.getVaultMethod(permsPath, "playerRemove", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("playerRemoveGroup", ReflectionUtil.getVaultMethod(permsPath, "playerRemoveGroup", String.class, OfflinePlayer.class, String.class));
            perm.methods.put("playerRemoveTransient", ReflectionUtil.getVaultMethod(permsPath, "playerRemoveTransient", String.class, OfflinePlayer.class, String.class));

            instance = perm;
        } catch (Exception e) {
            log.error("Error on init " + PermissionAdapter.class.getSimpleName() + "!");
            e.printStackTrace();
        }
    }

    public static boolean hasInit() {
        return instance != null;
    }

    public static PermissionAdapter getInstance() {
        return instance;
    }

    public String getCurrentPermissionPluginName() {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getName"), this.perms);
    }

    public boolean currnetPermissionPluginIsEnabled() {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("isEnabled"), this.perms);
    }

    public boolean playerHas(String name, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerHas"), this.perms, WorldUtil.getAllNames(), PlayerUtil
                .getOfflinePlayer(name), perm);
    }

    public String[] getGroups() {
        return (String[]) ReflectionUtil.invokeSafety(this.methods.get("getGroups"), this.perms);
    }

    public String getPrimaryGroup(String name) {
        return (String) ReflectionUtil.invokeSafety(this.methods.get("getPrimaryGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name));
    }

    public boolean groupHas(String groupName, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("groupHas"), this.perms, WorldUtil.getAllNames(), groupName, perm);
    }

    public boolean groupAdd(String groupName, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("groupAdd"), this.perms, WorldUtil.getAllNames(), groupName, perm);
    }

    public String[] getPlayerGroups(String name) {
        return (String[]) ReflectionUtil.invokeSafety(this.methods.get("getPlayerGroups"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name));
    }

    public boolean groupRemove(String groupName, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("groupRemove"), this.perms, WorldUtil.getAllNames(), groupName, perm);
    }

    public boolean playerAdd(String name, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerAdd"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), perm);
    }

    public boolean playerOnlineAddGroup(String uuid, String groupName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerAddGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getBukkitPlayer(uuid), groupName);
    }

    public boolean playerAddGroup(String playerName, String groupName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerAddGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(playerName), groupName);
    }

    public boolean playerAddTransient(String name, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerAddTransient"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), perm);
    }

    public boolean playerOnlineInGroup(String uuid, String groupName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerInGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getBukkitPlayer(uuid), groupName);
    }

    public boolean playerInGroup(String playerName, String groupName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerInGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(playerName), groupName);
    }

    public boolean playerRemove(String name, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerRemove"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), perm);
    }

    public boolean playerOnlineRemoveGroup(String uuid, String groupName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerRemoveGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getBukkitPlayer(uuid), groupName);
    }

    public boolean playerRemoveGroup(String playerName, String groupName) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerRemoveGroup"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(playerName), groupName);
    }

    public boolean playerRemoveTransient(String name, String perm) {
        return (Boolean) ReflectionUtil.invokeSafety(this.methods.get("playerRemoveTransient"), this.perms, WorldUtil.getAllNames(), PlayerUtil.getOfflinePlayer(name), perm);
    }

}
